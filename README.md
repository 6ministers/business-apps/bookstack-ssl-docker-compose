# Installing BookStack+SSL with docker-compose.

## Quick Installation

**Before starting the instance, direct the domain (subdomain) to the ip of the server where BookStack will be installed!**

## 1. Server Requirements
From and more
- 1 CPUs
- 1 RAM 
- 10 Gb 

Run for Ubuntu 22.04

``` bash
sudo apt-get purge needrestart
```

Install docker and docker-compose:

``` bash
curl -s https://gitlab.com/6ministers/business-apps/bookstack-ssl-docker-compose/-/raw/master/setup.sh | sudo bash -s

```

Download instance:


``` bash
curl -s ttps://gitlab.com/6ministers/business-apps/bookstack-ssl-docker-compose/-/raw/master/download.sh | sudo bash -s bookstack
```

If `curl` is not found, install it:

``` bash
$ sudo apt-get install curl
# or
$ sudo yum install curl
```

Go to the catalog

``` bash
cd bookstack
```

To change the domain in the `Caddyfile` to your own

``` bash
#Bookstack
https://bookstack.domains.com:443 {
    reverse_proxy 127.0.0.1:6875
	# tls admin@example.org
	encode zstd gzip
	file_server
...	
}
```


Добавить запись домена в `docker-compose.yml`
``` bash
  # Bookstack 
  bookstack:
    image: lscr.io/linuxserver/bookstack
    container_name: bookstack
    environment:
      - PUID=1000
      - PGID=1000
      - APP_URL=https://bookstack.domain.com
```

**Run app:**

``` bash
docker-compose up -d
```

Then open 
`https://bookstack.domain.com`

Default login email: `admin@admin.com`

Default login password: `password`

## Container management

**Run **:

``` bash
docker-compose up -d
```

**Restart**:

``` bash
docker-compose restart
```

**Restart**:

``` bash
sudo docker-compose down && sudo docker-compose up -d
```

**Stop**:

``` bash
docker-compose down
```

## Documentation

https://www.bookstackapp.com/docs/admin/installation/#docker
